Please install the following packages for successful execution of the pipeline code -
- git
- maven
- unzip
- java-1.8.0-openjdk*
May have to install the packages with a trailing * to include additional packages.

The pipeline required two arguments -
- pipeline to be ran - "build" or "release"
- Repository Path - "https://harshil5@bitbucket.org/harshil5/scripting-helloworld.git"

The pipeline can be executed only using the command -
./pipeline <pipeline name> <repository path>
Example:
./pipeline build https://harshil5@bitbucket.org/harshil5/scripting-helloworld.git

The pipeline requires the presence of two files -
- pipeline (Required for initial verification of the command)
- execution.py (Required for execution of the pipeline)

Logs of the execution are available depending on the outcome at two locations -
- logs.txt (at the current folder location/contains logs pertaining to the pipeline file)
- <repository folder>/logs.txt (at the repository folder/contains logs pertaining to the execution.py file)
