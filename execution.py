#!/usr/bin/env python

import yaml
import json
import sys
import os

#Extracting the yaml file into a simple json format
path = sys.argv[2]
os.chdir("./" + path)
with open("pipeline.yml", 'r') as stream:
    try:
        data = yaml.load(stream)
    except yaml.YAMLError as exc:
        print("Error in pipeline.yml file\nExiting")
        exit()

pipeline = sys.argv[1]
task = []
dic = {}
var1,var2 = 0, 0

#Verifying the presence of the correct pipeline argument
if pipeline == "build" or pipeline == "release":
    for pipe in data:
        if pipe == "pipelines":
            for key in data[pipe]:
                for value in key:
#Extracting the tasks specific to the pipeline
                    if pipeline == value:
                        for i in key[value]:
                            task.append(i)
#Executing the pipeline in the given repository branch
	elif pipe == "branch":
            var1 = os.system("git checkout " + data[pipe] + "&>> logs.txt")
	    if var1 == 0:
		print("Using the code in " + data[pipe] + " branch")
	    else:
		print("Invalid branch name " + data[pipe] + "\nExiting")
		exit()
#Forming a dictionary of tasks and commands to be executed for simplicity
        elif pipe == "tasks":
            for key in data[pipe]:
                for value in key:
                    for i in key[value]:
                        dic[value] = key[value][i]
    print("Executing the " + str(pipeline) + " pipeline")
#Executing the tasks in the pipeline and verifying their outputs
    for cmd in task:
        if cmd in dic:
            print("Running the " + str(cmd) + " task")
            var2 = os.system(dic[cmd] + "&>> logs.txt")
	    if var2 == 0:
                print("Successfully completed the " + str(cmd) + " task")
            else:
                print("Something went wrong in the " + str(cmd) + " task")
		print("Check the logs.txt file in the " + path + " folder for additional details")
		print("Exiting the execution of " + str(pipeline) + " pipeline")
		exit()
	else:
	    print("No valid task '" + str(cmd) + "' found to execute")
	    print("Failed to execute "+ str(pipeline) + " pipeline")
	    exit()
    print("Successfully executed the " + str(pipeline) + " pipeline")
else:
    print("You have input an invalid pipeline execution command '" + str(pipeline) + "'")
    print("Please try again with valid inputs like 'build' or 'release'")
